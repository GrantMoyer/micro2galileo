CXX=g++
CXXFLAGS+= -std=c++11 -Wall -pthread -lcurl

SRCDIR=src
OBJDIR=obj

DEPS=$(SRCDIR)/DSController.h \
	 $(SRCDIR)/SensorController.h \
     $(SRCDIR)/Time.h \
	 $(SRCDIR)/Light.h \
	 $(SRCDIR)/GpioController.h \
	 $(SRCDIR)/SafeBool.h \
	 $(SRCDIR)/ServerController.h \

sensor: $(OBJDIR)/main.o $(OBJDIR)/DSController.o $(OBJDIR)/SensorController.o $(OBJDIR)/Time.o $(OBJDIR)/Light.o $(OBJDIR)/GpioController.o $(OBJDIR)/SafeBool.o $(OBJDIR)/ServerController.o
	$(CXX) -o $@ $^ $(CXXFLAGS)

$(OBJDIR)/%.o: $(SRCDIR)/%.cpp $(DEPS) $(OBJDIR)
	$(CXX) -c -o $@ $< $(CXXFLAGS)

$(OBJDIR):
	mkdir -p $(OBJDIR)

.PHONY: clean

clean:
	rm -rf $(OBJDIR)
	rm -f sensor

