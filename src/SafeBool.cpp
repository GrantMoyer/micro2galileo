#include <iostream>

#include "SafeBool.h"

bool SafeBool::get() const {
	int err = pthread_mutex_lock(&readWrite);
	if (err < 0) {
		std::cerr << "Error Locking SafeBool Mutex" << std::endl;
	}
	bool temp = val;
	err = pthread_mutex_unlock(&readWrite);
	if (err < 0) {
		std::cerr << "Error Unlocking SafeBool Mutex" << std::endl;
	}
	return temp;
}

void SafeBool::set(bool val){
	int err = pthread_mutex_lock(&readWrite);
	if (err < 0) {
		std::cerr << "Error Locking SafeBool Mutex" << std::endl;
	}
	this->val = val;
	err = pthread_mutex_unlock(&readWrite);
	if (err < 0) {
		std::cerr << "Error Locking SafeBool Mutex" << std::endl;
	}
}
