#include <unistd.h>
#include <cstdio>
#include <cstdlib>
#include <iostream>

#include "SensorController.h"
#include "Light.h"
#include "GpioController.h"
#include "SafeBool.h"

using GpioController::setPin;
using GpioController::getPin;
using GpioController::exportPin;
using GpioController::unExportPin;
using GpioController::setPinIn;
using GpioController::PIN_0;
using GpioController::PIN_1;
using GpioController::PIN_2;
using GpioController::PIN_3;
using GpioController::PIN_0_PULL;
using GpioController::PIN_1_PULL;
using GpioController::PIN_2_PULL;
using GpioController::PIN_3_PULL;
using GpioController::PIN_STROBE;
using GpioController::PIN_LED;


SensorController::SensorController() {
	exportPin(PIN_0); //galileo
	exportPin(PIN_1); //galileo
	exportPin(PIN_2); //galileo
	exportPin(PIN_3); //galileo
	exportPin(PIN_0_PULL); //galileo
	exportPin(PIN_1_PULL); //galileo
	exportPin(PIN_2_PULL); //galileo
	exportPin(PIN_3_PULL); //galileo
	exportPin(PIN_STROBE); //galileo
	exportPin(PIN_LED); //galileo
	setPin(PIN_0_PULL, false); //galileo
	setPin(PIN_1_PULL, false); //galileo
	setPin(PIN_2_PULL, false); //galileo
	setPin(PIN_3_PULL, false); //galileo
	setPin(PIN_STROBE, false); //Strobe must initially be HIGH //galileo
	setPin(PIN_LED, false); //Strobe must initially be HIGH //galileo

	srand(284123984);

	pthread_create(&thread, NULL, &SensorControllerFriends::run, this);
}

SensorController::~SensorController() {
	unExportPin(PIN_STROBE); //galileo
	unExportPin(PIN_0); //galileo
	unExportPin(PIN_1); //galileo
	unExportPin(PIN_2); //galileo
	unExportPin(PIN_3); //galileo
	unExportPin(PIN_0_PULL); //galileo
	unExportPin(PIN_1_PULL); //galileo
	unExportPin(PIN_2_PULL); //galileo
	unExportPin(PIN_3_PULL); //galileo
}

void *SensorControllerFriends::run(void *voidSensorController) {
	SensorController *sensorController = (SensorController *) voidSensorController;

	while(true) {
		sensorController->pingSensor();
		sensorController->querySensor();
		usleep(10000); // sleep 10 ms
	}

	return NULL;
}

Brightness SensorController::getBrightness() const {
	return light.get();
}

bool SensorController::isAlive() const {
	return alive.get();
}

void SensorController::setCommand(Msg msg) {
	setPin(PIN_0, msg & 0x1); //galileo
	setPin(PIN_1, msg & 0x2); //galileo
	setPin(PIN_2, msg & 0x4); //galileo
	setPin(PIN_3, msg & 0x8); //galileo
}

void SensorController::querySensor() {
	usleep(5000); //galileo
	setPin(PIN_STROBE, false); //galileo
	setCommand(MSG_GET); //galileo
	setPin(PIN_STROBE, true); //galileo
	usleep(10000); //galileo
	setPin(PIN_STROBE, false); //galileo
	setBusIn(); //galileo
 //galileo
	usleep(5000); //galileo
	setPin(PIN_STROBE, true); //galileo
	usleep(5000); //galileo
	setPin(PIN_STROBE, false); //galileo
 //galileo
	Brightness val = 0; //galileo
	for(int i = 0; i < 3; ++i) { //galileo
		usleep(5000); //galileo
		setPin(PIN_STROBE, true); //galileo
		val += getBus() << 4 * i; //galileo
		setPin(PIN_STROBE, false); //galileo
	} //galileo
 //galileo
	usleep(5000); //galileo
	setPin(PIN_STROBE, true); //galileo
	usleep(5000); //galileo
	if (getBus() != MSG_ACK) resetSensor(); //galileo
 //galileo
	setPin(PIN_STROBE, false); //galileo
	usleep(5000); //galileo
	setPin(PIN_STROBE, true); //galileo
 //galileo
	if (val > 300) setPin(PIN_LED, true); //galileo
	else setPin(PIN_LED, false); //galileo

	light.set(val);
}

void SensorController::resetSensor() {
	usleep(5000); //galileo
	setPin(PIN_STROBE, false); //galileo
	setCommand(MSG_RESET); //galileo
	setPin(PIN_STROBE, true); //galileo
	usleep(10000); //galileo
	setPin(PIN_STROBE, false); //galileo
	setBusIn(); //galileo
}

void SensorController::pingSensor() {
	usleep(5000); //galileo
	setPin(PIN_STROBE, false); //galileo
	setCommand(MSG_PING); //galileo
	setPin(PIN_STROBE, true); //galileo
	usleep(10000); //galileo
	setPin(PIN_STROBE, false); //galileo
	setBusIn(); //galileo
 //galileo
	usleep(5000); //galileo
	setPin(PIN_STROBE, true); //galileo
	usleep(5000); //galileo
	setPin(PIN_STROBE, false); //galileo
 //galileo
	usleep(5000); //galileo
	setPin(PIN_STROBE, true); //galileo
	usleep(5000); //galileo
 //galileo
	unsigned val = getBus(); //galileo
 //galileo
	setPin(PIN_STROBE, false); //galileo
	usleep(5000); //galileo
	setPin(PIN_STROBE, true); //galileo
 //galileo
	alive.set(val == MSG_ACK);
}

std::uint_fast8_t SensorController::getBus() {
	unsigned val = 0x0; //galileo
	if(getPin(PIN_0)) val |= 0x1; //galileo
	if(getPin(PIN_1)) val |= 0x2; //galileo
	if(getPin(PIN_2)) val |= 0x4; //galileo
	if(getPin(PIN_3)) val |= 0x8; //galileo
 //galileo
	return val; //galileo

	return 0;
}

void SensorController::setBusIn() {
	setPinIn(PIN_0); //galileo
	setPinIn(PIN_1); //galileo
	setPinIn(PIN_2); //galileo
	setPinIn(PIN_3); //galileo
}
