#include <iostream>

#include "Light.h"

Brightness Light::get() const {
	int err = pthread_mutex_lock(&lightLock);
	if (err < 0) {
		std::cerr << "Error Locking Mutex" << std::endl;
	}
	Brightness val = brightness;
	err = pthread_mutex_unlock(&lightLock);
	if (err < 0) {
		std::cerr << "Error Unocking Mutex" << std::endl;
	}
	return val;
}

void Light::set(Brightness val){
	int err = pthread_mutex_lock(&lightLock);
	if (err < 0) {
		std::cerr << "Error Locking Mutex" << std::endl;
	}
	brightness = val;
	err = pthread_mutex_unlock(&lightLock);
	if (err < 0) {
		std::cerr << "Error Locking Mutex" << std::endl;
	}
}
