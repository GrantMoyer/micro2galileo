#ifndef GPIOCONTROLLER_H
#define GPIOCONTROLLER_H

namespace GpioController {
	enum Pin {
		PIN_0      = 48, //pin 14 is gpio13
		PIN_1      = 50, //pin 15 is gpio14
		PIN_2      = 52, //pin 16 is gpio6
		PIN_3      = 54, //pin 17 is gpio0
		PIN_0_PULL = 49,
		PIN_1_PULL = 51,
		PIN_2_PULL = 53,
		PIN_3_PULL = 55,
		PIN_STROBE = 40, //pin 18 is gpio1
		PIN_LED = 58, //pin 18 is gpio1
		PIN_I2C = 60,
	};

	void setPin(Pin pin, bool active);
	bool getPin(Pin pin);
	void exportPin(Pin pin);
	void unExportPin(Pin pin);

	std::string getFilename(Pin pin);
	void setPinOut(Pin pin);
	void setPinIn(Pin pin);
}

#endif
