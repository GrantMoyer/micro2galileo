#include <cstdio>
#include <string>
#include <iostream>

#include "GpioController.h"

void GpioController::exportPin(Pin pin) {
	char pinString[10];
	snprintf(pinString, 10, "%d", pin);
	
	FILE *expor = std::fopen("/sys/class/gpio/export", "w");
	std::fprintf(expor, pinString);
	std::fclose(expor);
}

void GpioController::unExportPin(Pin pin) {
	char pinString[10];
	snprintf(pinString, 10, "%d", pin);
	
	FILE *expor = std::fopen("/sys/class/gpio/unexport", "w");
	std::fprintf(expor, pinString);
	std::fclose(expor);
}

std::string GpioController::getFilename(Pin pin) {
	char pinString[10];
	snprintf(pinString, 10, "%d", pin);

	std::string filename("/sys/class/gpio/gpio");
	filename += pinString;
	filename += "/";

	return filename;
}

void GpioController::setPin(Pin pin, bool active) {
	setPinOut(pin);

	std::string filename = getFilename(pin);
	FILE *value = std::fopen((filename + "value").c_str(), "w");
	std::fprintf(value, active ? "1" : "0");
	std::fclose(value);
}

void GpioController::setPinOut(Pin pin) {
	std::string filename = getFilename(pin);
	FILE *direction = std::fopen((filename + "direction").c_str(), "w");
	if (direction != NULL) {
		std::fprintf(direction, "out\n");
		std::fclose(direction);
	}
}

bool GpioController::getPin(Pin pin) {
	setPinIn(pin);

	std::string filename = getFilename(pin);

	char gottenChar;

	FILE *value = std::fopen((filename + "value").c_str(), "r");
	if (value == NULL) std::cout << "cant find " << (filename + "value") << std::endl;
	gottenChar = fgetc(value);
	std::fclose(value);

	return (gottenChar != '0');
}

void GpioController::setPinIn(Pin pin) {
	std::string filename = getFilename(pin);
	FILE *direction = std::fopen((filename + "direction").c_str(), "w");
	if (direction != NULL) {
		std::fprintf(direction, "in\n");
		std::fclose(direction);
	}
}

