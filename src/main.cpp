/*
 * main.cpp controls the galileo, allowing it to connect to the light sensor.
 */

#include <string>
#include <iostream>
#include <pthread.h>

#include "DSController.h"
#include "SensorController.h"
#include "ServerController.h"
#include "Light.h"
#include "Time.h"

int main(int argc, char *argv[]) {
	SensorController sensorController;
	DSController dsController;
	ServerController serverController(&dsController, &sensorController);
	while(true) {
		std::cout << "Chu' want? ";

		std::string command;
		std::cin >> command;

		if (command == "help") {
			std::cout
				<< "Available commands:" << std::endl
				<< "help - displays this message" << std::endl
				<< "status - displays the status of the sensor and clock" << std::endl
				<< "exit - exits the program" << std::endl
			;
		} else if (command == "status") {
			std::cout
				<< "Ping: " << (sensorController.isAlive() ? "True" : "False") << std::endl
				<< "ADC Value: " << sensorController.getBrightness() << std::endl
				<< "Time: " << dsController.getTime() << std::endl
			;
		} else if (command == "exit") {
			break;
		} else {
			std::cout
				<< "Invalid command: '" << command << "'" << std::endl
			;
		}
	}
}

