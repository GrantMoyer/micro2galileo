#include <linux/i2c-dev.h> //galileo
#include <sys/ioctl.h>
#include <fcntl.h>
#include <unistd.h>
#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <ctime>
#include <cstring>
#include <pthread.h>

#include "DSController.h"
#include "Time.h"
#include "GpioController.h"

DSController::DSController() {
	// Set up i2c
	GpioController::exportPin(GpioController::PIN_I2C); //galileo
	GpioController::setPin(GpioController::PIN_I2C, false); //turn on I2C //galileo
 //galileo
	unsigned adapterNum = getAdapterNumber(); //galileo
	char filename[100]; //galileo
 //galileo
	snprintf(filename, sizeof(filename), "/dev/i2c-%d", adapterNum); //galileo
	fd = open(filename, O_RDWR); //galileo
	if (fd < 1) { //galileo
		std::cerr << "Error opening i2c device file" << std::endl; //galileo
		std::exit(1); //galileo
	} //galileo
 //galileo
	unsigned DEVICE_ADDRESS = 0x68; //galileo
	int err = ioctl(fd, I2C_SLAVE, DEVICE_ADDRESS); //galileo
	if (err < 0) { //galileo
		std::cerr << "Error setting device address" << std::endl; //galileo
		std::exit(1); //galileo
	} //galileo

	std::time_t time;
	std::time(&time);

	std::tm tm = *std::localtime(&time);

	Time now(tm);

	setTime(now);

	pthread_create(&thread, NULL, &DSControllerFriends::run, this);
}

DSController::~DSController() {
	if (close(fd) < 0) {
		std::cerr << "Error closing i2c device file" << std::endl;
		std::exit(1);
	}

	// Turn off i2c
	GpioController::unExportPin(GpioController::PIN_I2C);
}

void *DSControllerFriends::run(void *dsController) {
	DSController *controller = (DSController *) dsController;

	while(true) {
		controller->queryTime();
		usleep(500000); //wait 0.5 seconds
	}

	return NULL;
}

void DSController::setTime(const Time& time) {
	// Copy time array into command buffer
	std::uint8_t init[1 + 7] = {0x00};

	time.getAll(&init[1]);

	init[1] &= 0x7f;
	init[3] &= 0xbf;

	write(fd, init, sizeof(init)); //galileo
}

void DSController::queryTime() {
	const std::uint8_t ADDRESS[1] = {0x00}; //galileo
 //galileo
	write(fd, &ADDRESS, sizeof(ADDRESS)); //galileo
 //galileo
	std::uint8_t buffer[7]; //galileo
 //galileo
	read(fd, buffer, sizeof(buffer)); //galileo
 //galileo
	buffer[0] &= 0x7f; //galileo
	buffer[2] &= 0xbf; //galileo

	time.setAll(buffer);
}

Time DSController::getTime() const {
	return time;
}
unsigned DSController::getAdapterNumber() {
	return 0;
}

std::uint8_t DSController::bcdTob(std::uint8_t bcd) {
	return (bcd >> 4) * 10 + (bcd % 0x10);
}
