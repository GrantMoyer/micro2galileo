#ifndef TIME_H
#define TIME_H

#include <iostream>
#include <cstdint>
#include <ctime>
#include <pthread.h>

class Time {
public:
	Time();
	Time(std::tm tm);
	Time(const Time& time);
	~Time();

	void getAll(std::uint8_t *buffer) const;

	void setAll(std::uint8_t const *buffer);

private:
	std::uint8_t second;
	std::uint8_t minute;
	std::uint8_t hour;
	std::uint8_t day;
	std::uint8_t date;
	std::uint8_t month;
	std::uint8_t year;

	mutable pthread_mutex_t readWrite;
};

std::ostream& operator<<(std::ostream& os, const Time& time);

#endif
