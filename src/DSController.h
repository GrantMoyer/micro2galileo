#ifndef DSCONTROLLER_H
#define DSCONTROLLER_H

#include <pthread.h>

#include "Time.h"

namespace DSControllerFriends {
	void *run(void *dsController);
}

class DSController {
public:
	DSController();
	~DSController();

	Time getTime() const;

private:
	void setTime(const Time& t);
	void queryTime();
	unsigned getAdapterNumber();
	friend void *DSControllerFriends::run(void *dsController);

	pthread_t thread;
	Time time;
	unsigned char bcdTob(unsigned char bcd);
	int fd;

	const unsigned DS_ADDR = 0x68;
};

#endif
