#ifndef LIGHT_H
#define LIGHT_H

#include <pthread.h>
#include <cstdint>

typedef std::uint_fast16_t Brightness;

class Light {
public:
	Light() {pthread_mutex_init(&lightLock, NULL);}
	~Light() {pthread_mutex_destroy(&lightLock);}

	Brightness get() const;
	void set(Brightness);

private:
	Brightness brightness;
	mutable pthread_mutex_t lightLock;
};


#endif
