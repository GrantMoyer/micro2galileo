#include "Time.h"
#include <iostream>
#include <iomanip>
#include <string>

Time::Time() {
	int err = pthread_mutex_init(&readWrite, NULL);

	if (err < 0) {
		std::cerr << "Unable to initialize Time mutex" << std::endl;
	}
}

Time::Time(std::tm tm) :
	second(tm.tm_sec),
	minute(tm.tm_min),
	hour  (tm.tm_hour),
	day   (tm.tm_wday + 1),
	date  (tm.tm_mday),
	month (tm.tm_mon + 1),
	year  (tm.tm_year - 100)
{
	int err = pthread_mutex_init(&readWrite, NULL);

	if (err < 0) {
		std::cerr << "Unable to initialize Time mutex" << std::endl;
	}
}

Time::Time(const Time& time) {
	uint8_t buffer[7];

	time.getAll(buffer);

	second = buffer[0];
	minute = buffer[1];
	hour   = buffer[2];
	day    = buffer[3];
	date   = buffer[4];
	month  = buffer[5];
	year   = buffer[6];

	int err = pthread_mutex_init(&readWrite, NULL);

	if (err < 0) {
		std::cerr << "Unable to initialize Time mutex" << std::endl;
	}
}

Time::~Time() {
	int err = pthread_mutex_destroy(&readWrite);

	if (err < 0) {
		std::cerr << "Unable to initialize Time mutex" << std::endl;
	}
}

void Time::getAll(std::uint8_t *buffer) const {
	int err = pthread_mutex_lock(&readWrite);

	if (err < 0) {
		std::cerr << "Error locking Time mutex" << std::endl;
	}

	buffer[0] = second;
	buffer[1] = minute;
	buffer[2] = hour;
	buffer[3] = day;
	buffer[4] = date;
	buffer[5] = month;
	buffer[6] = year;

	err = pthread_mutex_unlock(&readWrite);

	if (err < 0) {
		std::cerr << "Error unlocking Time mutex" << std::endl;
	}
}

void Time::setAll(std::uint8_t const *buffer) {
	int err = pthread_mutex_lock(&readWrite);

	if (err < 0) {
		std::cerr << "Error locking Time mutex" << std::endl;
	}

	second = buffer[0];
	minute = buffer[1];
	hour = buffer[2];
	day = buffer[3];
	date = buffer[4];
	month = buffer[5];
	year = buffer[6];

	err = pthread_mutex_unlock(&readWrite);

	if (err < 0) {
		std::cerr << "Error unlocking Time mutex" << std::endl;
	}
}

std::ostream& operator<<(std::ostream& os, const Time& time) {
	char previous = os.fill('0');

	uint8_t buffer[7];

	time.getAll(buffer);

	std::string day;
	switch (buffer[3]) {
		case 1: day = "Sunday"; break;
		case 2: day = "Monday"; break;
		case 3: day = "Tuesday"; break;
		case 4: day = "Wednesday"; break;
		case 5: day = "Thursday"; break;
		case 6: day = "Friday"; break;
		case 7: day = "Saturday"; break;
		default: day = "Uh oh";
	}

	std::string month;
	switch (buffer[5]) {
		case  1: month = "January"; break;
		case  2: month = "Februrary"; break;
		case  3: month = "March"; break;
		case  4: month = "April"; break;
		case  5: month = "May"; break;
		case  6: month = "June"; break;
		case  7: month = "July"; break;
		case  8: month = "August"; break;
		case  9: month = "September"; break;
		case 10: month = "October"; break;
		case 11: month = "November"; break;
		case 12: month = "December"; break;
		default: month = "Oops";
	}

	os
		<< day << ", "
		<< month << " "
		<< std::setw(4) << (buffer[6] + 2000u) << "-"
		<< std::setw(2) << static_cast<unsigned>(buffer[5]) << "-"
		<< std::setw(2) << static_cast<unsigned>(buffer[4]) << " "
		<< std::setw(2) << static_cast<unsigned>(buffer[2]) << ':'
		<< std::setw(2) << static_cast<unsigned>(buffer[1]) << ':'
		<< std::setw(2) << static_cast<unsigned>(buffer[0]);
	os.fill(previous);
	return os;
}
