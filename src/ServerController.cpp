#include <iostream>
#include <pthread.h>
#include <unistd.h>
#include <curl/curl.h>

#include "ServerController.h"

constexpr unsigned ServerController::id;
constexpr char ServerController::password[];
constexpr char ServerController::name[];

constexpr char ServerController::urlFormat[];
constexpr char ServerController::dateFormat[];

ServerController::ServerController(
	const DSController *dsController,
	const SensorController *sensorController
) :
	dsController(dsController),
	sensorController(sensorController)
{
	curl = curl_easy_init();
	if(curl != NULL) {
		curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, &write_callback);
	} else {
		std::cerr << "Error initializing curl" << std::endl;
	}

	pthread_create(&thread, NULL, &ServerControllerFriends::run, this);
}

ServerController::~ServerController() {
	curl_easy_cleanup(curl);
}

size_t ServerController::write_callback(char *ptr, size_t size, size_t nmemb, void *userdata) {
	return size * nmemb;
}

void *ServerControllerFriends::run(void *voidServerController) {
	ServerController *serverController = (ServerController *) voidServerController;

	while (true) {
		serverController->send(
			serverController->dsController->getTime(),
			serverController->sensorController->getBrightness(),
			serverController->sensorController->isAlive()
		);
		usleep(10000000); // wait 10 seconds between server requests
	}

	return NULL;
}

void ServerController::send(Time time, Brightness brightness, bool alive) {
	uint8_t buffer[7];
	time.getAll(buffer);

	char date[100];
	snprintf(date, sizeof(date), dateFormat, buffer[6] + 2000, buffer[5], buffer[4], buffer[2], buffer[1], buffer[0]);

	char url[1024];
	snprintf(url, sizeof(url), urlFormat, id, password, name, brightness, date, alive ? "Ping+succeeded" : "Ping+failed");

	curl_easy_setopt(curl, CURLOPT_URL, url);
	curl_easy_perform(curl);
}

