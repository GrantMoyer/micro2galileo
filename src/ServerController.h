#ifndef SERVERCONTROLLER_H
#define SERVERCONTROLLER_H

#include <string>
#include <pthread.h>
#include <stdint.h>
#include <inttypes.h>
#include <curl/curl.h>

#include "DSController.h"
#include "SensorController.h"
#include "Light.h"
#include "Time.h"

namespace ServerControllerFriends {
	void *run(void *);
}

class ServerController {
public:
	ServerController(const DSController *, const SensorController *);
	~ServerController();

private:
	static constexpr unsigned id = 3;
	static constexpr char password[] = "02DjJyEprr";
	static constexpr char name[] = "Morty";

	static constexpr char urlFormat[] = "http://ec2-54-152-121-129.compute-1.amazonaws.com:8080/update?id=%u&password=%s&name=%s&data=%u&timestamp=%s&status=%s";
	static constexpr char dateFormat[] = "%04u-%02u-%02u+%02u:%02u:%02u";

	const DSController *dsController;
	const SensorController *sensorController;

	CURL *curl;

	pthread_t thread;

	friend void *ServerControllerFriends::run(void *);
	void send(Time, Brightness, bool);
	static size_t write_callback(char *, size_t, size_t, void *);
};

#endif // SERVERCONTROLLER_H

