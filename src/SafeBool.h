#ifndef SAFEBOOL_H
#define SAFEBOOL_H

#include <pthread.h>

class SafeBool {
public:
	SafeBool() {pthread_mutex_init(&readWrite, NULL);}
	~SafeBool() {pthread_mutex_destroy(&readWrite);}

	bool get() const;
	void set(bool);

private:
	bool val;
	mutable pthread_mutex_t readWrite;
};

#endif
