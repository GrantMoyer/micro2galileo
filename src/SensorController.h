#ifndef SENSORCONTROLLER_H
#define SENSORCONTROLLER_H

#include <cstdint>
#include <string>

#include "Light.h"
#include "SafeBool.h"

namespace SensorControllerFriends {
	void *run(void *voidSensorController);
}

class SensorController {
private:
	enum Msg {
		MSG_RESET   = 0x0,
		MSG_PING    = 0x1,
		MSG_GET     = 0x2,
		MSG_ACK     = 0xE,
		MSG_NOTHING = 0xF,
	};

public:
	SensorController();
	~SensorController();

	Brightness getBrightness() const;
	bool isAlive() const;

private:
	friend void *SensorControllerFriends::run(void *voidSensorController);
	pthread_t thread;

	Light light;
	SafeBool alive;

	// Communication
	void resetSensor();
	void pingSensor();
	void querySensor();
	void setCommand(Msg msg);
	std::uint_fast8_t getBus();
	void setBusIn();
};

#endif
